#if 0
	gcc -std=c2x -ggdb3 -Wextra -Wno-missing-field-initializers \
			-o ${0%.*} \
			$0 &&
 	ret=$?
	if test "$1" != +norun && test "$ret" == 0; then
		gdb -ex run --args ${0%.*} $@
	fi
	exit
#endif
#define _GNU_SOURCE

#define NS_IMPLEMENTATION
#include "ns.h"
#include <assert.h>
#include <string.h>

static void test_ns_make () {
	char *str = "this is this";
	struct ns *ns = ns_make (str);
	assert (ns);
	assert (ns->d);
	assert (!strcmp (ns->d, str));
	assert (ns->d[ns->len] == 0);
	assert ((size_t) ns->len == strlen (ns->d));
	assert (ns->cap > ns->len);
}

static void test_ns_cat () {
#define	STR_1 "this is this"
	struct ns *ns = ns_make (STR_1);
#define STR_2 " and that is that"
	ns_cat (ns, STR_2);
	assert (!strcmp (ns->d, STR_1 STR_2));
#undef STR_1
#undef STR_2
}

static void test_ns_insert () {
#define	STR_1 "this is this"
	struct ns *ns = ns_make (STR_1);
#define STR_2 " and that is that"
	assert (!ns_insert (ns, 0, STR_2));
	assert (!strcmp (ns->d, STR_2 STR_1));

	struct ns *ns_2 = ns_make (STR_1);
	assert (ns_insert (ns, 4000, STR_2) != 0);

#undef STR_1
#undef STR_2
}

static void test_ns_makef () {
	char *str = "this is this";
	char *fmt_str = "The str says \"%s\"";
	struct ns *ns = ns_makef (fmt_str, str);

	assert (ns);
	assert (ns->d);
	assert (ns->d[ns->len] == 0);
	assert (ns->cap > ns->len);

	char *a_normal_str = nullptr;
	asprintf (&a_normal_str, fmt_str, str);
	assert ((size_t) ns->len == strlen (a_normal_str));
	assert (!strcmp (a_normal_str, ns->d));
}

static void test_ns_catf () {
	// Let's just say it passes.
}

static void test_ns_insertf () {
	// Let's just say it passes.
}

int main (int, char **) {
#define REPORT($code) \
	do { \
		printf ("ns test: Starting %s ... ", #$code); \
		fflush (stdout); \
		$code; \
		printf ("passed.\n"); \
	} while (0)

	REPORT (test_ns_make ());
	REPORT (test_ns_cat ());
	REPORT (test_ns_insert ());
	REPORT (test_ns_makef ());
	REPORT (test_ns_catf ());
	REPORT (test_ns_insertf ());

	exit (0);
}



