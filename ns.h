#define __GNU_SOURCE

#include <stdio.h>

typedef struct ns ns;
struct ns {

	// Doesn't include the 0. There's always a 0 at the end.
	ssize_t len;
	ssize_t cap;
	char d[];
};


struct ns *ns_make (const char *c_str);
void ns_cat (ns *ns, const char *c_str);
int ns_insert (ns *ns, int at, const char *c_str);
struct ns *ns_makef (char *fmt, ...);
void ns_catf (ns *ns, const char *fmt, ...);
int ns_insertf (ns *ns, int at, const char *fmt, ...);

#ifdef NS_IMPLEMENTATION

#include <stdlib.h>
#include <stdarg.h>

size_t c_str_len (const char *c_str) {
	int r = 0;
	while (c_str[r])
		r++;
	return r;
}

static inline void *__ns_mempcpy (void *_dest, const void *_src, size_t n) {
	char *dest = _dest;
	const char *src = _src;
	for (size_t i = 0; i < n; i++)
		dest[i] = src[i];
	return (char *) _dest + n;
}

static inline void __ns_grow (ns **ns, ssize_t how_much) {
	if ((*ns)->len + how_much > (*ns)->cap) {
		(*ns)->cap *= 2;
		*ns = realloc (*ns, sizeof **ns + (*ns)->cap);
	}
}

struct ns *ns_make (const char *c_str) {
	size_t len = c_str_len (c_str);

	// Seems reasonable to over-allocate. Why would you use a ns like
	// this if you weren't planning to append to it?
	size_t cap = (len + 1) * 2;

	ns *r = malloc (sizeof *r + cap);
	*r = (ns) {
		.cap = cap,
		.len = len,
	};
	__ns_mempcpy (r->d, c_str, len + 1);
	return r;
}

void ns_cat (ns *ns, const char *c_str) {
	int len = c_str_len (c_str);
	__ns_grow (&ns, len);
	__ns_mempcpy (ns->d + ns->len, c_str, len + 1);
	ns->len += len;
}

int ns_insert (ns *ns, int at, const char *c_str) {
	if (at > ns->len)
		return 1;
	int len = c_str_len (c_str);
	__ns_grow (&ns, len);

	// Make space.
	__ns_mempcpy (ns->d + at + len, ns->d + at, ns->len - at);

	// Do copy.
	__ns_mempcpy (ns->d + at, c_str, len);
	ns->len += len;
	return 0;
}

struct ns *ns_makef (char *fmt, ...) {
	char *str = nullptr;
	va_list ap;
	va_start (ap);
	vasprintf (&str, fmt, ap);
    struct ns *r = ns_make (str);
	va_end (ap);
	free (str);
	return r;
}

void ns_catf (ns *ns, const char *fmt, ...) {
	char *str = nullptr;
	va_list ap;
	va_start (ap);
	vasprintf (&str, fmt, ap);
    ns_cat (ns, str);
	va_end (ap);
	free (str);
}

int ns_insertf (ns *ns, int at, const char *fmt, ...) {
	char *str = nullptr;
	va_list ap;
	va_start (ap);
	vasprintf (&str, fmt, ap);
    int r = ns_insertf (ns, at, str);
	va_end (ap);
	free (str);
	return r;
}
#endif
